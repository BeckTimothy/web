FROM node:lts-alpine as build

USER root
RUN mkdir -p /app
WORKDIR /app

COPY package.json package-lock.json ./
RUN npm ci --silent
COPY src ./src
COPY public ./public
COPY tsconfig.json craco.config.js .env ./
RUN npm run build

FROM nginx:stable-alpine

COPY --from=build /app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
