const CracoLessPlugin = require('craco-less');

module.exports = {
    plugins: [
        {
            plugin: CracoLessPlugin,
            options: {
                lessLoaderOptions: {
                    lessOptions: {
                        modifyVars: {
                            '@primary-color': '#a65e2a',
                            '@body-background': '#090909',
                        },
                        javascriptEnabled: true,
                    },
                },
            },
        },
    ],
};
