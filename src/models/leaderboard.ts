export interface Leaderboard {
    data: Player[];
    skip: number;
    take: number;
    sync_delay: number;
}

export interface Player {
    name: string;
    rank: number;
    rating: number;
    last_update: number;
    favorite_weapon_item: string;
    favorite_weapon_name: string;
    twitch_username?: string;
}
