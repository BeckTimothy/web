export interface PlayerInfo {
    name: string;
    elo_1v1?: number;
    rank_1v1?: number;
    fav_weapon?: string;
    fav_weapon_1v1?: string;
}


export interface Player2V2BattleInfo {
    partners: Partner[];
    weapons: Weapons[];
}

export interface Partner {
    name: string;
    count: number;
    wins: number;
    win_rate: number;
}

export interface Weapons {
    weapon_1: string;
    weapon_2: string;
    wins: number;
}
