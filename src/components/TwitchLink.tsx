import React from 'react';

export const TwitchLink = ({ link }: { link: string | null }) => {
    if (!link) {
        return null
    }

    return (
        <a href={link} target="_blank" rel="noreferrer">
            <img src="/twitch.png" alt="twitch" />
        </a>
    );
}

