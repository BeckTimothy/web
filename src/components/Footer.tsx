import React from 'react';
import { Layout } from 'antd';
import { Link } from 'react-router-dom';

export const Footer = () => {
    return (
        <Layout.Footer style={{ textAlign: 'center' }}>
            <div className="footer">
                <span><Link to="/about">About</Link></span>
                <span><a href="https://gitlab.com/albion-murder-ledger">Source Code</a></span>
                <span><a href="https://discord.gg/NrWHJsWXdz">Discord</a></span>
            </div>
        </Layout.Footer>
    );
};
