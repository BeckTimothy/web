import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Layout } from 'antd';

import { AppBar } from 'components/AppBar';
import { routes } from 'routes';
import { Footer } from 'components/Footer';

function App() {
    return (
        <Router>
            <Layout>
                <header>
                    <Switch>
                        {routes.map((route, index) => (
                            <Route key={index} path={route.path} children={<AppBar />} />
                        ))}
                    </Switch>
                </header>
                <Layout.Content style={{ padding: '0 50px', minHeight: '84vh' }}>
                    <Switch>
                        {routes.map((route, index) => (
                            <Route key={index} path={route.path} children={<route.main />} />
                        ))}
                    </Switch>
                </Layout.Content>
                <Footer />
            </Layout>
        </Router>
    );
}

export default App;
