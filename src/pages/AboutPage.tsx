import React from 'react';
import { Typography } from 'antd';

const { Title, Paragraph, Link } = Typography;

export const AboutPage = () => (
    <div style={{ display: 'flex', justifyContent: 'center', paddingTop: '30px' }}>
        <Typography style={{ maxWidth: '800px' }}>
            <Title>About</Title>
            <Paragraph>
                Albion Murder Ledger is an open source project I started working on in my free time.
                The the project is primarily educational.
                There other Albion stats websites that may provide better information in some areas.
                The main focus of Albion Murder Ledger is to show a quick and as up-to-date as possible kill history.
                I would love to collaborate with anyone who would like to help improve the app or add features.
            Reach out on <Link href="https://discord.gg/NrWHJsWXdz">Discord</Link> or <Link href="https://gitlab.com/albion-murder-ledger">Gitlab</Link> if you're interested!
        </Paragraph>
        </Typography>
    </div>
);
