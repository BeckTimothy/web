import React, { useEffect, useState, useMemo } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Table, Spin, Select, Space } from 'antd';

import { BuildStats, BuildStat, Build, buildId } from 'models/build_stats';
import { GenericItem } from 'components/Item'
import { api } from 'api';

type BuildStatsPageProps = {
    name: string
}

export const BuildStatsPage = ({ name }: BuildStatsPageProps) => {
    const history = useHistory();
    const { search: queryString } = useLocation();
    const queryParams = useMemo(() => new URLSearchParams(queryString), [queryString])

    const [stats, setStats] = useState<BuildStats | null>(null);
    const initLookbackDays = queryParams.get('lookback_days') ? parseInt(queryParams.get('lookback_days') ?? '30') : 30;
    const [lookbackDays, setLookbackDays] = useState<number>(initLookbackDays);

    useEffect(() => {
        const params = new URLSearchParams()

        if (lookbackDays !== 30) {
            params.append("lookback_days", lookbackDays.toString());
        } else {
            params.delete("lookback_days");
        }
        api<BuildStats>(`/players/${name}/stats/builds?${params.toString()}`)
            .then(stats => {
                setStats(stats);
                history.replace({ search: params.toString() });
            });
    }, [name, lookbackDays, history]);

    const filters = useMemo(() => {
        const notice = lookbackDays >= 9999 ? (<span>Data collection began on Jan 9th 2021</span>) : null;

        return (
            <Space>
                <Select
                    value={lookbackDays}
                    defaultValue={30}
                    style={{ width: 140 }}
                    onSelect={v => setLookbackDays(v)}
                >
                    <Select.Option value={30}>Last 30 Days</Select.Option>
                    <Select.Option value={7}>Last 7 Days</Select.Option>
                    <Select.Option value={14}>Last 14 Days</Select.Option>
                    <Select.Option value={9999}>All Time</Select.Option>
                </Select>
                {notice}
            </Space>
        );
    }, [lookbackDays]);

    if (stats === null) {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', marginTop: '80px' }}>
                <Spin size="large" />
            </div>
        );
    }

    return (
        <Space direction="vertical" style={{ width: '100%' }}>
            {filters}
            <Table
                pagination={false}
                scroll={{ x: true }}
                dataSource={stats.builds}
                size="small"
                rowKey={(stat: BuildStat) => buildId(stat.build)}
            >
                <Table.Column
                    title="Build"
                    dataIndex="build"
                    key="build"
                    align="center"
                    render={(build: Build) => (
                        <Space>
                            <GenericItem item={build.main_hand.type} size={50} tooltip={build.main_hand.en_name} />
                            <GenericItem item={build.off_hand.type} size={50} tooltip={build.off_hand.en_name} />
                            <GenericItem item={build.head.type} size={50} tooltip={build.head.en_name} />
                            <GenericItem item={build.body.type} size={50} tooltip={build.body.en_name} />
                            <GenericItem item={build.shoe.type} size={50} tooltip={build.shoe.en_name} />
                            <GenericItem item={build.cape.type} size={50} tooltip={build.cape.en_name} />
                        </Space>
                    )}
                />
                <Table.Column
                    title="Usages"
                    dataIndex="usages"
                    key="usages"
                    sorter={(a: BuildStat, b: BuildStat) => a.usages - b.usages}
                    defaultSortOrder="descend"
                />
                <Table.Column
                    title="Fame Ratio"
                    dataIndex="fame_ratio"
                    key="fame_ratio"
                    sorter={(a: BuildStat, b: BuildStat) => (a.fame_ratio ?? 0) - (b.fame_ratio ?? 0)}
                />
                <Table.Column
                    title="Win Rate"
                    dataIndex="win_rate"
                    key="win_rate"
                    render={(v: number) => (v * 100).toFixed(1).toString() + '%'}
                    sorter={(a: BuildStat, b: BuildStat) => a.win_rate - b.win_rate}
                />
                <Table.Column
                    title="Kills"
                    dataIndex="kills"
                    key="kills"
                    sorter={(a: BuildStat, b: BuildStat) => a.kills - b.kills}
                />
                <Table.Column
                    title="Deaths"
                    dataIndex="deaths"
                    key="deaths"
                    sorter={(a: BuildStat, b: BuildStat) => a.deaths - b.deaths}
                />
                <Table.Column
                    title="Assists"
                    dataIndex="assists"
                    key="assists"
                    sorter={(a: BuildStat, b: BuildStat) => a.assists - b.assists}
                />
                <Table.Column
                    title="Kill Fame"
                    dataIndex="kill_fame"
                    key="kill_fame"
                    render={(v: number) => v.toLocaleString()}
                    sorter={(a: BuildStat, b: BuildStat) => a.kill_fame - b.kill_fame}
                />
                <Table.Column
                    title="Death Fame"
                    dataIndex="death_fame"
                    key="death_fame"
                    render={(v: number) => v.toLocaleString()}
                    sorter={(a: BuildStat, b: BuildStat) => a.death_fame - b.death_fame}
                />
                <Table.Column
                    title="Average IP"
                    dataIndex="average_item_power"
                    key="average_item_power"
                    render={(v: number) => v.toFixed(0)}
                    sorter={(a: BuildStat, b: BuildStat) => a.average_item_power - b.average_item_power}
                />
            </Table>
        </Space>
    );
};
