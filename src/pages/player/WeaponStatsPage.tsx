import React, { useEffect, useState, useMemo } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Table, Spin, Select, Space } from 'antd';

import { WeaponStats, WeaponStat } from 'models/weapon_stats';
import { GenericItem } from 'components/Item'
import { api } from 'api';

type WeaponStatsPageProps = {
    name: string
}

export const WeaponStatsPage = ({ name }: WeaponStatsPageProps) => {
    const history = useHistory();
    const { search: queryString } = useLocation();
    const queryParams = useMemo(() => new URLSearchParams(queryString), [queryString])

    const [stats, setStats] = useState<WeaponStats | null>(null);
    const initLookbackDays = queryParams.get('lookback_days') ? parseInt(queryParams.get('lookback_days') ?? '30') : 30;
    const [lookbackDays, setLookbackDays] = useState<number>(initLookbackDays);

    useEffect(() => {
        const params = new URLSearchParams()

        if (lookbackDays !== 30) {
            params.append("lookback_days", lookbackDays.toString());
        } else {
            params.delete("lookback_days");
        }
        api<WeaponStats>(`/players/${name}/stats/weapons?${params.toString()}`)
            .then(stats => {
                setStats(stats);
                history.replace({ search: params.toString() });
            });
    }, [name, lookbackDays, history]);

    const filters = useMemo(() => {
        const notice = lookbackDays >= 9999 ? (<span>Data collection began on Jan 9th 2021</span>) : null;

        return (
            <Space>
                <Select
                    value={lookbackDays}
                    defaultValue={30}
                    style={{ width: 140 }}
                    onSelect={v => setLookbackDays(v)}
                >
                    <Select.Option value={30}>Last 30 Days</Select.Option>
                    <Select.Option value={7}>Last 7 Days</Select.Option>
                    <Select.Option value={14}>Last 14 Days</Select.Option>
                    <Select.Option value={9999}>All Time</Select.Option>
                </Select>
                {notice}
            </Space>
        );
    }, [lookbackDays]);

    if (stats === null) {
        return (
            <div style={{ display: 'flex', justifyContent: 'center', marginTop: '80px' }}>
                <Spin size="large" />
            </div>
        );
    }

    return (
        <Space direction="vertical" style={{ width: '100%' }}>
            {filters}
            <Table size="small" pagination={false} scroll={{ x: true }} dataSource={stats.weapons} rowKey={w => w.weapon}>
                <Table.Column
                    title="Weapon"
                    dataIndex="weapon"
                    key="weapon"
                    align="center"
                    render={weapon => (
                        <GenericItem item={weapon} size={50} />
                    )}
                />
                <Table.Column
                    title="Usages"
                    dataIndex="usages"
                    key="usages"
                    sorter={(a: WeaponStat, b: WeaponStat) => a.usages - b.usages}
                    defaultSortOrder="descend"
                />
                <Table.Column
                    title="Fame Ratio"
                    dataIndex="fame_ratio"
                    key="fame_ratio"
                    sorter={(a: WeaponStat, b: WeaponStat) => (a.fame_ratio ?? 0) - (b.fame_ratio ?? 0)}
                />
                <Table.Column
                    title="Win Rate"
                    dataIndex="win_rate"
                    key="win_rate"
                    render={(v: number) => (v * 100).toFixed(1).toString() + '%'}
                    sorter={(a: WeaponStat, b: WeaponStat) => a.win_rate - b.win_rate}
                />
                <Table.Column
                    title="Kills"
                    dataIndex="kills"
                    key="kills"
                    sorter={(a: WeaponStat, b: WeaponStat) => a.kills - b.kills}
                />
                <Table.Column
                    title="Deaths"
                    dataIndex="deaths"
                    key="deaths"
                    sorter={(a: WeaponStat, b: WeaponStat) => a.deaths - b.deaths}
                />
                <Table.Column
                    title="Assists"
                    dataIndex="assists"
                    key="assists"
                    sorter={(a: WeaponStat, b: WeaponStat) => a.assists - b.assists}
                />
                <Table.Column
                    title="Kill Fame"
                    dataIndex="kill_fame"
                    key="kill_fame"
                    render={(v: number) => v.toLocaleString()}
                    sorter={(a: WeaponStat, b: WeaponStat) => a.kill_fame - b.kill_fame}
                />
                <Table.Column
                    title="Death Fame"
                    dataIndex="death_fame"
                    key="death_fame"
                    render={(v: number) => v.toLocaleString()}
                    sorter={(a: WeaponStat, b: WeaponStat) => a.death_fame - b.death_fame}
                />
                <Table.Column
                    title="Average IP"
                    dataIndex="average_item_power"
                    key="average_item_power"
                    render={(v: number) => v.toFixed(0)}
                    sorter={(a: WeaponStat, b: WeaponStat) => a.average_item_power - b.average_item_power}
                />
            </Table>
        </Space>
    );
};
