import React, { useEffect, useState, useMemo } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Spin, Select, Space } from 'antd';

import { WeaponMatrix } from 'components/WeaponMatrix';
import { WeaponMatrix as WeaponMatrixModel, MatchupStats } from 'models/weapon_matrix';
import { api } from 'api';
import { weapons } from 'models/weapons';
import { filterDict } from 'util/filterDict';

export const WeaponMatrixPage = () => {
    const history = useHistory();
    const { search: queryString } = useLocation();
    const queryParams = useMemo(() => new URLSearchParams(queryString), [queryString])

    const [weapon, setWeapon] = useState<string>(queryParams.get('weapon') ?? '');
    const [isSlayer, setIsSlayer] = useState<string>(queryParams.get('slayer') ?? '1');

    const [matrix, setMatrix] = useState<WeaponMatrixModel | null>(null);

    useEffect(() => {
        const params = new URLSearchParams()
        params.set('slayer', isSlayer);

        api<{ matrix: WeaponMatrixModel }>(`/weapon-matrix?${params.toString()}`)
            .then(r => {
                let matrix = r.matrix;

                Object.keys(r.matrix).map(key => {
                    return matrix[key] = filterDict((m: MatchupStats) => {
                        return m.wins + m.losses > 15;
                    }, matrix[key]);
                });

                matrix = filterDict((w: { [key: string]: MatchupStats }) => {
                    return Object.keys(w).length > 10;
                }, matrix);

                setMatrix(matrix);
            });
    }, [isSlayer]);

    useEffect(() => {
        const params = new URLSearchParams()

        if (isSlayer !== '1') {
            params.append("slayer", isSlayer);
        } else {
            params.delete("slayer");
        }

        if (weapon !== '') {
            params.append('weapon', weapon);
        } else {
            params.delete('weapon');
        }

        history.replace({ search: params.toString() })
    }, [weapon, isSlayer, history]);

    const filters = useMemo(() => {
        return (
            <Space>
                <div>
                    <div>Data Set</div>
                    <Select
                        value={isSlayer}
                        defaultValue=""
                        style={{ width: 240 }}
                        onSelect={v => setIsSlayer(v)}
                    >
                        <Select.Option value="1">Slayer</Select.Option>
                        <Select.Option value="0">All</Select.Option>
                    </Select>
                </div>
                <div>
                    <div>Weapon</div>
                    <Select
                        showSearch
                        defaultValue=""
                        allowClear
                        value={weapon}
                        onSelect={v => { setWeapon(v) }}
                        style={{ width: 180 }}
                        onClear={() => setWeapon('')}
                        /* @ts-ignore */
                        filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                        {Object.entries(weapons).map(([val, label]) => (
                            <Select.Option key={val} value={val}>{label}</Select.Option>
                        ))}
                    </Select>
                </div>
            </Space>
        );
    }, [weapon, isSlayer]);

    const weaponMatrixView = useMemo(() => {
        if (matrix === null) {
            return (
                <div style={{ display: 'flex', justifyContent: 'center', marginTop: '80px' }}>
                    <Spin size="large" />
                </div>
            );
        }

        return (
            <WeaponMatrix matrix={matrix} weapon={weapon} />
        )
    }, [matrix, weapon]);

    return (
        <Space direction="vertical" style={{ width: '100%' }}>
            <h1>1v1 Weapon Matrix (last 7 days)</h1>
            {filters}
            {weaponMatrixView}
        </Space>
    );
};
