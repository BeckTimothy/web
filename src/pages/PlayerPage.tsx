import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Tabs } from 'antd';

import { api } from 'api';
import { PlayerInfo } from 'models/player_info';
import { LedgerPage } from 'pages/player/LedgerPage';
import { Battle2v2Page } from 'pages/player/Battle2v2Page';
import { WeaponStatsPage } from 'pages/player/WeaponStatsPage';
import { BuildStatsPage } from 'pages/player/BuildStatsPage';
import { MatchupsPage } from 'pages/player/MatchupsPage';
import { MMRChartPage } from 'pages/player/MMRChartPage';
import { PlayerHeader } from 'components/PlayerHeader';

export const PlayerPage = () => {
    const history = useHistory();
    const { name, tab } = useParams<{ name: string, tab: string }>();
    const [playerInfo, setPlayerInfo] = useState<PlayerInfo>({ name });

    useEffect(() => {
        setPlayerInfo({ name });
        api<PlayerInfo>(`/players/${name}/info`)
            .then(info => {
                setPlayerInfo(info);
            })
            .catch(err => {
                console.log(err);
            });
    }, [name]);

    return (
        <div>
            <PlayerHeader player={playerInfo} />
            <Tabs
                activeKey={tab}
                defaultActiveKey={tab}
                onChange={t => history.replace(`/players/${name}/${t}`)}
            >
                <Tabs.TabPane tab="Ledger" key="ledger">
                    {tab === 'ledger' ? <LedgerPage name={name} /> : null}
                </Tabs.TabPane>
                <Tabs.TabPane tab="2v2s" key="2v2s">
                    {tab === '2v2s' ? <Battle2v2Page name={name} /> : null}
                </Tabs.TabPane>
                <Tabs.TabPane tab="Weapons" key="weapons">
                    {tab === 'weapons' ? <WeaponStatsPage name={name} /> : null}
                </Tabs.TabPane>
                <Tabs.TabPane tab="Builds" key="builds">
                    {tab === 'builds' ? <BuildStatsPage name={name} /> : null}
                </Tabs.TabPane>
                <Tabs.TabPane tab="Matchups (1v1)" key="matchups">
                    {tab === 'matchups' ? <MatchupsPage name={name} /> : null}
                </Tabs.TabPane>
                <Tabs.TabPane tab="MMR Chart" key="mmr-chart">
                    {tab === 'mmr-chart' ? <MMRChartPage name={name} /> : null}
                </Tabs.TabPane>
            </Tabs>
        </div>
    );
}
