import React, { useState, useMemo } from 'react';
import { Spin, Space, Select, Input, Empty } from 'antd';
import { useHistory, useLocation } from 'react-router-dom';
import humanizeDuration from 'humanize-duration';

import useInfiniteScroll from 'react-infinite-scroll-hook';
import { useDebounceEffect } from 'util/useDebounceEffect';
import { api } from 'api'
import { Event } from 'components/Event';
import { Event as EventModel } from 'models/event';
import { weapons } from 'models/weapons';

let isLoading = false;
export const VodsPage = () => {
    const history = useHistory();

    const { search: queryString } = useLocation();
    const queryParams = useMemo(() => new URLSearchParams(queryString), [queryString])

    const [events, setEvents] = useState<EventModel[]>([]);
    const [syncDelay, setSyncDelay] = useState<number>(0);
    const [loading, setLoading] = useState<boolean>(true);
    const [hasNextPage, setHasNextPage] = useState<boolean>(true);
    const [q, setQ] = useState<string>(queryParams.get('q') ?? '');
    const [battleSize, setBattleSize] = useState<string>(queryParams.get('battle_size') ?? '');
    const [weapon, setWeapon] = useState<string>(queryParams.get('own_weapon') ?? '');

    const loadMore: (clear: boolean) => void = async (clear = false) => {
        if (isLoading) {
            return;
        }
        isLoading = true;
        const params = new URLSearchParams()

        if (q) {
            params.append("q", q);
        } else {
            params.delete("q");
        }
        if (battleSize) {
            params.append("battle_size", battleSize);
        } else {
            params.delete("battle_size");
        }

        if (weapon) {
            params.append("weapon", weapon);
        } else {
            params.delete("weapon");
        }

        if (clear) {
            history.replace({ search: params.toString() })
        }

        params.set('skip', clear ? '0' : events.length.toString());
        setLoading(true);
        const result = await api<{ events: EventModel[], sync_delay: number }>(`/vod-events?${params.toString()}`);
        if (clear) {
            setEvents(result.events);
        } else {
            setEvents(prev => [...prev, ...result.events]);
        }
        setHasNextPage(result.events.length === 20);
        setSyncDelay(result.sync_delay);
        setLoading(false);
        isLoading = false;
    };

    const infiniteRef = useInfiniteScroll({
        loading,
        hasNextPage,
        onLoadMore: loadMore,
    });

    useDebounceEffect(
        () => loadMore(true),
        300,
        [q, battleSize, weapon],
    );

    const syncDelayEl = useMemo(() => {
        if (loading) {
            return null;
        }

        const delay = humanizeDuration(syncDelay * 1000, { units: ["d", "h", "m"], round: true });

        if (syncDelay > 1800) {
            return (
                <div title="Albion API is running behind" style={{ color: 'red' }}>Sync Delay: {delay}</div>
            );
        }

        return (
            <div>Sync Delay: {delay}</div>
        );
    }, [syncDelay, loading]);

    const filters = useMemo(() => {
        return (
            <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between', alignItems: 'end' }}>
                <Space size="large" direction="horizontal" wrap>
                    <div>
                        <div>Opponent Search</div>
                        <Input.Search
                            value={q}
                            allowClear
                            placeholder="Search for opponent"
                            onChange={e => setQ(e.target.value)} style={{ width: 220 }}
                        />
                    </div>
                    <div>
                        <div>Battle Size</div>
                        <Select
                            value={battleSize}
                            defaultValue=""
                            allowClear
                            style={{ width: 140 }}
                            onSelect={v => setBattleSize(v)}
                            onClear={() => setBattleSize('')}
                        >
                            <Select.Option value="">Any Fight Size</Select.Option>
                            <Select.Option value="1v1">1v1</Select.Option>
                            <Select.Option value="2v2">2v2</Select.Option>
                            <Select.Option value="5v5">5v5</Select.Option>
                            <Select.Option value="zvz">ZvZ</Select.Option>
                        </Select>
                    </div>
                    <div>
                        <div>Weapon</div>
                        <Select
                            showSearch
                            defaultValue=""
                            allowClear
                            value={weapon}
                            onSelect={v => setWeapon(v)}
                            style={{ width: 180 }}
                            onClear={() => setWeapon('')}
                            /* @ts-ignore */
                            filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        >
                            {Object.entries(weapons).map(([val, label]) => (
                                <Select.Option key={val} value={val}>{label}</Select.Option>
                            ))}
                        </Select>
                    </div>
                </Space>
                {syncDelayEl}
            </div>
        );
    }, [syncDelayEl, weapon, battleSize, q]);

    const eventElements = useMemo(() => {
        if (loading && events.length === 0) {
            return (
                <div style={{ display: 'flex', justifyContent: 'center', marginTop: '80px' }}>
                    <Spin size="large" />
                </div>
            );
        }
        if (events.length === 0) {
            return (<Empty style={{ marginTop: '80px' }} />)
        }

        return events.map(event => (<Event event={event} key={event.id} perspective="" />));
    }, [events, loading]);

    return (
        <Space direction="vertical" style={{ width: '100%' }}>
            <h1>Recently Streamed Fights</h1>
            {filters}
            <div ref={infiniteRef as React.LegacyRef<HTMLDivElement>}>
                {eventElements}
            </div>
        </Space>
    );
};
